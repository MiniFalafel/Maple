#pragma once

// Intended for users in applications

#include "Maple/Application.h"
#include "Maple/Layer.h"
#include "Maple/Log.h"

#include "Maple/Core/Timestep.h"

#include "Maple/Input.h"
#include "Maple/KeyCodes.h"
#include "Maple/MouseButtonCodes.h"

#include "Maple/ImGui/ImGuiLayer.h"

// ******************Rendering Headers*******************
#include "Maple/Renderer/Renderer.h"
#include "Maple/Renderer/RenderCommand.h"

#include "Maple/Renderer/Buffer.h"
#include "Maple/Renderer/Shader.h"
#include "Maple/Renderer/VertexArray.h"

#include "Maple/Renderer/OrthographicCamera.h"

// *********************Entry Point**********************
#include "Maple/EntryPoint.h"
// ******************************************************